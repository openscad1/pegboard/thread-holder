// BOF

// NO STL

include <thread-holder-defaults.scad>;
include <thread-holder-lib.scad>;

include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/mcube.scad>;

module bernina_bobbin_tray() {

    translate([0, -bernina_bobbin_tray_depth / 2, 0]) {

        difference() {

            mcube(size = [tray_width, bernina_bobbin_tray_depth, tray_height * 1.25],
                align = [0, 0, 1],
                chamfer = plate_thickness / 4,
                color = "salmon");

            translate([0, 0, (guterman_500m_spool_outer_diameter) / 2]) {

                color("red") {
                    for(x = [ -1.5 : 1.5 ]) {
                        translate([(guterman_500m_spool_width / 4 + plate_thickness/2) * x, 0, 0]) {

                            hull() {
                               scale([
                                    (bernina_bobbin_width + plate_thickness/2) / bernina_bobbin_width,
                                    (bernina_bobbin_outer_diameter + plate_thickness) / bernina_bobbin_outer_diameter,
                                    (bernina_bobbin_outer_diameter + plate_thickness) / bernina_bobbin_outer_diameter
                                ]) {
                                    bernina_bobbin_slug();
                                }
                            }
                        }
                    }
                }
           }
           if(!is_undef(debug_model)) {
              mcube([20, 100, 100], align = [0, 0, 0]);
              translate([29, 0, 0]) {
                  mcube([20, 100, 100], align = [0, 0, 0]);
              }

           }
            translate([0, 0, bernina_bobbin_outer_diameter * .85]) {
                rotate([0, 90, 0]) {
                    color("red") {
                        mtube(height = 100, od = bernina_bobbin_outer_diameter * .75, id=0);
                    }
                }
            }
        }


        if(!is_undef(debug_model)) {

            color("lightblue") {

                for(x = [ -1.5 : 1.5 ]) {
                    translate([(guterman_500m_spool_width / 4 + plate_thickness/2) * x, 0, bernina_bobbin_outer_diameter/2 + plate_thickness * 1.5]) {
                        bernina_bobbin_slug();
                    }
                }
            }
        }


    }
}

// debug_model = true;

if(!is_undef(debug_model)) {
    bernina_bobbin_tray();
}

// EOF
