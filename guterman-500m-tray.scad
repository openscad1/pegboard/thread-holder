// BOF

// NOSTL

include <thread-holder-defaults.scad>;
include <thread-holder-lib.scad>;

include <libopenscad/mcube.scad>;



module guterman_500m_tray() {
    translate([0, -guterman_500m_tray_depth * 1/2, 0]) {
        difference() {

            mcube(size = [tray_width, guterman_500m_tray_depth, tray_height * 1.25], 
                align = [0, 0, 1],
                chamfer = plate_thickness * 1/4, 
                color = "salmon");

            color("red") {     
                translate([0, 0, (guterman_500m_spool_outer_diameter + plate_thickness * 3) / 2]) 

                hull() {
                   scale([
                    (guterman_500m_spool_width + plate_thickness) / guterman_500m_spool_width, 
                    (guterman_500m_spool_outer_diameter + plate_thickness) / guterman_500m_spool_outer_diameter, 
                    (guterman_500m_spool_outer_diameter + plate_thickness) / guterman_500m_spool_outer_diameter]) guterman_500m_slug();
               }
         //     mcube([56/2, 100, 100], align = [0, 0, 0]);

            }

            
        }
        
        if(!is_undef(debug_model)) {
            color("lightblue") {
                    translate([0, 0, guterman_500m_spool_outer_diameter/2 + plate_thickness * 1.5]) {
                        guterman_500m_slug();
                    }
            }        
        }

    }
}

// debug_model = true;

if(!is_undef(debug_model)) {
    guterman_500m_tray();
}


// EOF
