// BOF

include <guterman-500m-tray.scad>;
include <bernina-bobbin-tray.scad>;
include <rail.scad>;


translate([0, 0, guterman_500m_spool_outer_diameter * 1.5 + plate_thickness/2 + 10.725]) {
    bernina_bobbin_tray();
// support
//
    translate([0, 0, plate_thickness]) {
        hull() {
            intersection() {
                mcube([100, 100, 27], align = [0, -1, -1]);
                union () {
                    translate([0, 0, -plate_thickness]) {
                        bernina_bobbin_tray();
                    }

                    translate([-bernina_bobbin_tray_depth / 2, 0, tray_width / 2 ])
                    translate([0, -plate_thickness, -bernina_bobbin_tray_depth])

                    rotate([90, 90, 180]) {
                        bernina_bobbin_tray();
                    }

                }
            }
        }
    }
}



guterman_500m_tray();


if(!is_undef(debug_model)) {
    translate([0, -guterman_500m_tray_depth * 3/4, guterman_500m_spool_outer_diameter/2 + tray_height + plate_thickness / 2]) {
        guterman_500m_slug();
    }
}


translate([0, 0.01, 0]) {
    rotate([0, 0, 180]) {
        color("blue") rail(height = 3, chamfer = 1, copies = 2);
    }
}


// EOF

