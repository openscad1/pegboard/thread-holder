// BOF

// NOSTL

include <libopenscad/mtube.scad>;
include <thread-holder-defaults.scad>;

module guterman_500m_slug() {
    color("pink") {
        rotate([0, 90, 0]) {
            mtube(height = guterman_500m_spool_width, od = guterman_500m_spool_outer_diameter, id = guterman_500m_spool_inner_diameter, chamfer = 0 /* 1/2 */);
        }
    }
}


module bernina_bobbin_slug() {
    color("pink") {
        rotate([0, 90, 0]) {
            mtube(height = bernina_bobbin_width, od = bernina_bobbin_outer_diameter, id = bernina_bobbin_inner_diameter, chamfer = 0 /* 1/2 */);
        }
    }
}

// EOF
